# Notas sobre el uso de Git.

{toc}

# Instalación de Git y Git Flow.
Para Windows descargar el Git de https://github.com/git-for-windows/git/releases, ya incluye el Git Flow.

Para Debian instalar el paquete git y el paquete git-flow a parte.

# Preconfiguraciones.
Es necesario realizas las siguientes acciones de configurar para tener el entorno preparado:


# Agregar acceso ssh a gitlab

## Generar claves seǵun documento 
Seguir los pasos del doc https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account

- Generar clave. ED25519 SSH keys	
```
ssh-keygen -t ed25519 -C "<comment>"
```
- Copiar clave generada a la configuración del usuario https://gitlab.com/-/profile/keys

```
xclip -sel clip < ~/.ssh/id_ed25519.pub
```

- Modificar repo remoto 

```
git remote set-url origin git@<user_1.gitlab.com>:gitlab-org/gitlab.git
```

## No aplicar fast-forward en "merges":
```
	git config --global gitflow.feature.finish.no-ff TRUE
```

## Configurar el usuario y el correo del git:
```
	git config --global user.name "NOMBRE APELLIDOS LDAP"
	git config --global user.email "CORREO-E REGISTRO EN GITLAB"
```


# Inicio de repositorio.
Como ejemplos de inicio de un repositorio en local podemos lanzas los siguientes comandos:

## Creación de un nuevo repositorio.

```
	git clone http://usuario.ldap@git.sas.junta-andalucia.es/rutarepo/repo.git
	cd directorio_repo
	touch README.md
	git add README.md
	git commit -m "agregado README"
	git push -u origin master
```

## Inclusión de un directorio existe en el repositorio.

```
	cd directorio_existente
	git init
	git remote add origin http://usuario.ldap@git.sas.junta-andalucia.es/rutarepo/repo.git
	git add .
	git commit -m "Inicio repositorio"
	git push -u origin master
```

Conexión a un repositorio existente.

```
	cd existing_repo
	git remote add origin http://usuario.ldap@git.sas.junta-andalucia.es/rutarepo/repo.git
	git push -u origin --all
	git push -u origin --tags
```

# Uso de GitFlow.

En el entorno de desarrollo local del proveedor, deberá estar clonado el repositorio GIT remoto del SAS con la misma estructura de ramas obligatoria (DEVELOP y MASTER). Siendo la rama de trabajo siempre, la rama DEVELOP. 

## Se deberá iniciar Git Flow.

```
	git flow init
```

Esta sentencia solo es necesario ejecutarla una vez en el repositorio local, mientras no se elimine y se cree un nuevo repositorio local.

El Git Flow nos pregunta sobre los nombres de las ramas y tomaremos los valores por defecto.

## Crear feature para hacer cambios.

Para cada una de las funcionales a desarrollar, se deberá crear una rama FEATURE desde la rama DEVELOP con GIT FLOW. Esta rama FEATURE deberá seguir la nomenclatura especificada en la normativa vigente (Normativa)

```
	git flow feature start "MYFEATURE"
```

Cuando se finalice el trabajo con la rama FEATURE que corresponda, agregadno ficheros y realizan commit de los mismos) se procederá a finalizar la rama con GIT FLOW. La finalización de dicha rama con GIT FLOW, realizará automáticamente lo siguiente:

```
	git flow feature finish "MYFEATURE"
```

1. Fusionar la rama FEATURE en cuestión con la rama DEVELOP.
2. Borrar la rama FEATURE en cuestión.
3. Cambiar a rama DEVELOP como rama de trabajo actual.

## Crear relase para versionar los cambios

Una vez la rama DEVELOP esté actualizada con todas las FEATURES que corresponda a la versión que se va a entregar, se deberá crear con GIT FLOW una rama RELEASE siguiendo lo especificado en la normativa vigente (Normativa).

```
	git flow release start "RELEASE"
```

La finalización con GIT FLOW de la rama RELEASE, realizará automáticamente lo siguiente:

```
	git flow release finish "RELEASE"
```

1. Fusionar la rama RELEASE con la rama MASTER.
2. Crea el TAG de la versión que corresponda desde la rama MASTER.
3. Fusionar la rama RELEASE con la rama DEVELOP.
4. Borrar la rama RELEASE en cuestión.

Por último, es muy importante no olvidar subir todas las ramas (incluidos los TAG) al repositorio GIT remoto del SAS.

## subir todas las ramas y todos los tags

```
	git push --tags origin master develop #incluir los nombres de las ramas que se suban
```
