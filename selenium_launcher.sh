#!/bin/bash
######################################
# ARRANQUE Y COMPROBACIÓN HEATLESS MODE   	      #
######################################

FECHA=`date '+%Y%m%d-%H%M%S'`
RUTALOG='/home/oca/utils/logs_selenium/'
RUTAAPP='/home/oca/utils/selenium-server/'
#APP='selenium-server-standalone-2.53.1.jar '
APP='selenium-server-standalone-3.5.1.jar '
SERVER='http://172.21.242.214'
CLIENT='172.21.242.214'
SERVER_PORT='4444'
CLIENT_PORT='4446'
DRIVER="/home/oca/utils/driver/geckodriver/geckodriver"
BROWSER="browserName=firefox,platform=LINUX"
BROWSER_APP="/usr/bin/firefox-esr"
#DRIVER="/home/oca/utils/driver/chromedriver/chromedriver"
#BROWSER="browserName=chrome,platform=LINUX"
#BROWSER_APP="/usr/bin/google-chrome"

#LOG=$RUTALOG"selenium_inicio.log"

getPID_Xvfb() {
local PID=`ps aux | grep Xvfb | grep -v grep | awk '{print $2}'`
echo $PID
}

getPID_SeleniumHub() {
local PID=`ps aux | grep $APP" -role hub" | grep -v grep | awk '{print $2}'`
echo $PID
}

getPID_SeleniumNode() {
local PID=`ps aux | grep $APP" -role node" | grep -v grep | awk '{print $2}'`
echo $PID
}

getPID_FirefoxESR() {
local PID=`ps aux | grep $BROWSER_APP | grep -v grep | awk '{print $2}'`
echo $PID
}

log() {
#echo $* >> $LOG
echo $*
}

######################################
#  COMPROBACIÓN ENTORNO				      #
######################################

log $RUTALOG
log $LOG
log $FECHA

PID=$(getPID_FirefoxESR)
if [ ! -z ${PID} ]
then
	log "existe proceso FIREFOX se cierra"
	kill $PID
	sleep 3s
else
	log "no existe proceso FIREFOX "
fi

PID=$(getPID_Xvfb)
if [ ! -z ${PID} ]
then
	log "existe proceso Xvfb"
else
	log "arrancado Xvfb"
	Xvfb :0 &> $RUTALOG'xvfb.log' &
	sleep 3s
fi

export DISPLAY=:0

PID=$(getPID_SeleniumHub)
if [ ! -z ${PID} ]
then
	log "existe proceso hub selenium"
else
	log "arrancando hub selenium"
	java -jar $RUTAAPP$APP -role hub -log $RUTALOG"selenium_hub.log" -port $SERVER_PORT &
	sleep 5s
fi


PID=$(getPID_SeleniumNode)
if [ ! -z ${PID} ]
then
	log "existe proceso node selenium" 
else
	log "arrancando node selenium"
	java -jar $RUTAAPP$APP -role node -log $RUTALOG"selenium_node.log" -hub $SERVER:$SERVER_PORT/grid/register -host $CLIENT -port $CLIENT_PORT -browser $BROWSER -Dwebdriver.gecko.driver=$DRIVER &
	sleep 5s
fi


log  "todo arrancado" 