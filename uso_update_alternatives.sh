#Agregar alternativas de Java.

#tener instalado el java en las rutas /usr/lib/jvm/VERSIONES_JAVA

#agregar Java 7 a las alternativas
sudo update-alternatives  --install /usr/bin/java java /usr/lib/jvm/jdk1.7.0_80/bin/java 2082

#    update-alternatives: utilizando /usr/lib/jvm/jdk1.7.0_80/bin/java para proveer /usr/bin/java (java) en modo automático

#mostrar alternativas disponibles de java
update-alternatives --display java

#    java - modo automático
#     la mejor versión del enlace es /usr/lib/jvm/jdk1.7.0_80/bin/java.
#     el enlace apunta actualmente a /usr/lib/jvm/jdk1.7.0_80/bin/java
#     el enlace java es /usr/bin/java
#     esclavo java.1.gz es /usr/share/man/man1/java.1.gz
#    /usr/lib/jvm/java-11-openjdk-amd64/bin/java - prioridad 1111
#     esclavo java.1.gz: /usr/lib/jvm/java-11-openjdk-amd64/man/man1/java.1.gz
#    /usr/lib/jvm/jdk1.7.0_80/bin/java - prioridad 2082

#agregar Java 8 a las alternativas
sudo update-alternatives  --install /usr/bin/java java /usr/lib/jvm/jdk1.8.0_201/bin/java 3092

#    update-alternatives: utilizando /usr/lib/jvm/jdk1.8.0_201/bin/java para proveer /usr/bin/java (java) en modo automático


update-alternatives --display java
java - modo automático
#     la mejor versión del enlace es /usr/lib/jvm/jdk1.8.0_201/bin/java.
#     el enlace apunta actualmente a /usr/lib/jvm/jdk1.8.0_201/bin/java
#     el enlace java es /usr/bin/java
#     esclavo java.1.gz es /usr/share/man/man1/java.1.gz
#    /usr/lib/jvm/java-11-openjdk-amd64/bin/java - prioridad 1111
#     esclavo java.1.gz: /usr/lib/jvm/java-11-openjdk-amd64/man/man1/java.1.gz
#    /usr/lib/jvm/jdk1.7.0_80/bin/java - prioridad 2082
#    /usr/lib/jvm/jdk1.8.0_201/bin/java - prioridad 3092

#configurar java a usar o mostrar el que esta activado
update-alternatives --config java

#    Existen 3 opciones para la alternativa java (que provee /usr/bin/java).
#    
#      Selección   Ruta                                         Prioridad  Estado
#    ------------------------------------------------------------
#    * 0            /usr/lib/jvm/jdk1.8.0_201/bin/java            3092      modo automático
#      1            /usr/lib/jvm/java-11-openjdk-amd64/bin/java   1111      modo manual
#      2            /usr/lib/jvm/jdk1.7.0_80/bin/java             2082      modo manual
#      3            /usr/lib/jvm/jdk1.8.0_201/bin/java            3092      modo manual
#    
#    Pulse <Intro> para mantener el valor por omisión [*] o pulse un número de selección: 

