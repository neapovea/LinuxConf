#!/bin/bash

#for j in 7 6 5 4 3 2 1 ; 
#do 
# echo $j
#done ; 

#20180728
# incluida ruta para copiar los que son iguales de nombre pero distinto de contenido.

#20180724
# agregadas variables contadores y control por diff

#20180723. 
# instalar paquete de manipulador de imagenes 
#   sudo apt install libimage-exiftool-perl
# revisar y lanzarlo para para todas la fotos.
# hay que revisar las variables siguientes

vMOVER=1
vDEBUG=0
vSOBREESCRIBIR=0

vRUTA_LOGS='/media/alejandromaillard/LNX/'
vRUTA_DESTINO='/media/alejandromaillard/LNX/destinoFotos/'
vDIR_PROCESO='/media/alejandromaillard/LNX/Media/'
vRUTA_DISTINTOS='/media/alejandromaillard/LNX/destinoFotos/distintos/'

#vRUTA_DESTINO='/media/alx/LNX/destinoFotos/'
#vDIR_PROCESO='/home/alx/Documentos/copia_movil_cely/'


cTOTALFICHEROS=0
cPROCESANDO=0
cMOVER=0
cSOBREESCRIBIR=0
cEXISTEDESTINO=0
cNOEXISTEFICHERO=0
cDIFERENTES=0


fREPETIDOS=$vRUTA_LOGS"log_ficheros_repetidos2.log"
fLOG=$vRUTA_LOGS"log_proceso2.log"
fNOIGUALES=$vRUTA_LOGS"log_no_iguales2.log"
# lanzar con ./renombrar_y_mover.v01.sh 2>&1 | tee -a fichero.log

log() 
{ 

	echo $* >> $fLOG 
	
	if test $vDEBUG -eq 1; then	echo $*; fi

}


echo 'INICIO----------------------------------------------------------------------------------------------------------'
log $vRUTA_DESTINO  
log $vDIR_PROCESO   
log 'INICIO----------------------------------------------------------------------------------------------------------'


cTOTALFICHEROS=$(ls -1UR "$vDIR_PROCESO" | wc -l)
directorios="$(find "$vDIR_PROCESO" -type "d")"

log "directorioss............................." $directorios

for directorio in $directorios; do

	log "..nombre directorio........." "$directorio"

	cd "$directorio"

	for ficheroOrigen in *.JPG *.jpg *.png *.PNG *.avi *.AVI *.mp4 *.m4v *.3gp *.WAV *.wav *.jpeg *.JPEG *.mpg *.MPG *.mov *.MOV;
	do

		
		if [ -f "$ficheroOrigen" ];
		then

			ahora=$(date)
			vDIR_DESTINO=''
			vRuta_tmp=''
			fecha_creacion=''
			fecha_corta=''
			destino=''
			fecha_modif_original=''
			fecha_modif_tmp=''
			fecha_modif_final=''
			fecha_crea_original=''
			fecha_crea_tmp=''
			fecha_crea_final=''


			#limpiar ficheros de espacios para tratarlos con el exiftool

			i=$(echo $ficheroOrigen | sed 's/ /_/g' | sed 's/://g'| sed 's/-//g'| sed 's/__/_/g' )

			if test "$ficheroOrigen" != "$i";
			then
				mv "$ficheroOrigen" $i
			fi

			#nombre_fichero=${ext_tmp##*/}
			nombre_fichero=$(basename "$i")
			ruta_fichero=${i%/*}
			extension=${i##*.}

			log "..PROCESANDO................" $cPROCESANDO " --- " $cTOTALFICHEROS
			log "..nombre fichero origen....." $ficheroOrigen  
			log "..nombre direcotrio origen.." "$directorio"
			log "..ruta fichero.............." $ruta_fichero  
			log "..extension fichero........." $extension  
			log "..nombre limpiado..........." $nombre_destino_limpiado


			vRuta_tmp=$(exiftool -T -createdate ""$i"" -d "%Y.%m")
			fecha_creacion=$(exiftool -T -createdate ""$i"" -d "%Y%m%d_%H%M%S")
			fecha_corta=$(exiftool -T -createdate ""$i"" -d "%Y%m%d_%H%M")

			log "..vRuta_tmp................." $vRuta_tmp  
			log "..fecha_creacion............" $fecha_creacion  
			log "..fecha_corta..............." $fecha_corta  
			

			fecha_modif_original=$(stat -c %y $i)
			fecha_modif_tmp=$(echo $fecha_modif_original | sed 's/ /_/g' | sed 's/://g'| sed 's/-//g'| sed 's/__//g')
			fecha_modif_final=$(expr substr $fecha_modif_tmp 1 15)


			log "..fecha_modif_original......" $fecha_modif_original  
			log "..fecha_modif_tmp..........." $fecha_modif_tmp  
			log "..fecha_modif_final........." $fecha_modif_final  

			fecha_crea_original=$(stat -c %w $i)
			fecha_crea_tmp=$(echo $fecha_crea_original | sed 's/ /_/g' | sed 's/://g'| sed 's/-//g'| sed 's/__//g')
			if test $(expr index '20' "$fecha_crea_tmp") -eq 0;
			then
				log $fecha_crea_tmp
			else

				fecha_crea_final=$(expr substr $fecha_crea_tmp 1 15)
				log $(expr substr $fecha_crea_tmp 1 15)
			fi
			

			log "..fecha_crea_original......." $fecha_crea_original  
			log "..fecha_crea_tmp............" $fecha_crea_tmp  
			log "..fecha_crea_final.........." $fecha_crea_final  

			nombre_destino_limpio=$(echo $i | sed 's/ /_/g' | sed 's/://g'| sed 's/-//g'| sed 's/__//g' )

			vDIR_DESTINO=$(echo $vRuta_tmp | sed 's/ /_/g' | sed 's/-//g' )

			log "..nombre_destino_limpio....." $nombre_destino_limpio  
			log "..vDIR_DESTINO imagen........"$vDIR_DESTINO  

			if test $(expr index '20' "$vDIR_DESTINO") -eq 0;
			then
				if test $(expr index '20' "$fecha_crea_final") -eq 0;
				then
					#echo $(expr substr $fecha_crea_final 1 4)"."$(expr substr $fecha_crea_final 5 2)
					vDIR_DESTINO=$(expr substr $fecha_modif_final 1 4)"."$(expr substr $fecha_modif_final 5 2)
					fecha_creacion=$fecha_modif_final
				else
					#echo $(expr substr $fecha_crea_final 1 4)"."$(expr substr $fecha_crea_final 5 2)
					vDIR_DESTINO=$(expr substr $fecha_crea_final 1 4)"."$(expr substr $fecha_crea_final 5 2)
					fecha_creacion=$fecha_crea_final
				fi
				fecha_corta=$(expr substr $fecha_creacion 1 13)

				log "..vDIR_DESTINO REVISADO....." $vDIR_DESTINO  
				
			fi



			if test $(expr index '20' "$vDIR_DESTINO") -eq 0;
			then
				log "..ERROR....algo fue mal con el DIRECTORIO " $vDIR_DESTINO " del fichero " $i 

			else
				#MOVER FICHERO A DIRECTORIO
				if [ -d $vRUTA_DESTINO""$vDIR_DESTINO ];
				then
					log "..existe ruta destino......." $vDIR_DESTINO
				else
					log "..no existe ruta destino...."  $vDIR_DESTINO
					#CREAR DIRECTORIO SI NO EXISTE
					mkdir $vRUTA_DESTINO""$vDIR_DESTINO
				fi


				log "..ruta destino.............." $vDIR_DESTINO  
				log "..fichero..................." $i  
				log "..fecha_creacion............" $fecha_creacion  
				log "..fecha_corta..............." $fecha_corta  
				log "..nombre_destino_limpio....." $nombre_destino_limpio  

				log "..nombre fichero origen....." $ficheroOrigen  
				log "..nombre limpiado..........." $nombre_destino_limpiado


				if (echo $nombre_destino_limpio | grep -sq $fecha_corta);
				then
					destinoFichero=$nombre_destino_limpio
					log "..el fichero ya con fecha..."  
				else
					destinoFichero=$fecha_creacion"__"$nombre_destino_limpio
					log "..el fichero ya SIN fecha..."  
				fi

				destinoDistinto=$vRUTA_DISTINTOS$cPROCESANDO"_"$destinoFichero
				destino=$vRUTA_DESTINO$vDIR_DESTINO"/"$destinoFichero

				log "..." $destino


				if test $vMOVER -eq 0;
				then
					log "..no mover.................."
				else
					if [ -f "$destino" ];
				    then
				        log "..EXISTE FICHERO DESTINO......"
						cEXISTEDESTINO=$((cEXISTEDESTINO+1))

						DIFF=$(diff $i $destino)

						if [ "$DIFF" != "" ];
						then
						    log "..FICHEROS NO SON IGUALES PERO EN EL NOMBRE SI ..................."

						    #mover el fichero a ruta distintos
							mv -v $i $destinoDistinto						   
							log "..mover..DISTINTO................"
						    echo "$directorio""/"$i >> $fNOIGUALES
							echo "#""$destino" >> $fNOIGUALES	    
							cDIFERENTES=$((cDIFERENTES+1))

						else
							echo "$directorio""/"$i >> $fREPETIDOS
							echo "#""$destino" >> $fREPETIDOS
						fi
					fi
					
					if test $vSOBREESCRIBIR -eq 0 && test -f "$destino";
					then
						log "..NO SE SOBREESCRIBE EL FICHERO........"
						cSOBREESCRIBIR=$((cSOBREESCRIBIR+1))
						#count=`expr $count + 1`
					else				
						mv -v $i $destino
						cMOVER=$((cMOVER+1))
						log "..mover....................."
					fi

				fi		
				log ".. origen " $i " destino " $destino 
			fi
			if test $vDEBUG -eq 1;
			then
				log "................................................................................................................"
			fi	
			log "..ahora....................." $ahora
		else
			log "..NO EXISTE FICHERO........." $ficheroOrigen
			cNOEXISTEFICHERO=$((cNOEXISTEFICHERO+1))
		fi

		cPROCESANDO=$((cPROCESANDO+1))

	done;
done;

log 'FINICIO---------------------------------------------------------------------------------------------------------'
log 'RESULTADOS...PROCESADOS -- TOTALES .........' $cPROCESANDO ' --- ' $cTOTALFICHEROS
log 'RESULTADOS...SE MUEVEN......................' $cMOVER
log 'RESULTADOS...NO SE SOBREESCRIBEN............' $cSOBREESCRIBIR
log 'RESULTADOS...EXISTE DESTINO...DIFERENTES....' $cEXISTEDESTINO ' --- ' $cDIFERENTES
log 'RESULTADOS...NO EXISTE FICHERO..............' $cNOEXISTEFICHERO
echo 'FINICIO---------------------------------------------------------------------------------------------------------'